var prices;
var quantities;
var res;
var total=0;
var country;
var taxes = new Map();
taxes.set('DE', 0.2);
taxes.set('UK', 0.21);
taxes.set('FR', 0.20);
taxes.set('IT', 0.25);
taxes.set('ES', 0.19);
taxes.set('PL', 0.21);
taxes.set('RO', 0.20);
taxes.set('NL', 0.20);
taxes.set('BE', 0.24);
taxes.set('EL', 0.2);
taxes.set('CZ', 0.19);
taxes.set('PT', 0.23);
taxes.set('HU', 0.27);
taxes.set('SE', 0.23);
taxes.set('AT', 0.22);
taxes.set('BG', 0.21);
taxes.set('DK', 0.21);
taxes.set('FI', 0.17);
taxes.set('SK', 0.18);
taxes.set('IE', 0.21);
taxes.set('HR', 0.23);
taxes.set('LT', 0.23);
taxes.set('SI', 0.24);
taxes.set('LV', 0.2);
taxes.set('EE', 0.22);
taxes.set('CY', 0.21);
taxes.set('LU', 0.25);
taxes.set('MT', 0.20);



exports.order = function order(req, res, next) {
  if(typeof req.body.country != 'string' || Array.isArray(req.body.prices) == false || Array.isArray(req.body.quantities) == false || typeof req.body.reduction != 'string' ){
    res.status(400).end();
    return;
  }
  // TODO implement from here
  console.log(req.body);
  prices = req.body.prices;
  quantities = req.body.quantities;
  if(prices.length != quantities.length){
    res.status(400).end();
    return;
  }
  total=0.0;
  for ( i = 0; i<prices.length; i++) {
    total = total + parseFloat(prices[i])*parseFloat(quantities[i]);
  }
  country = ''+req.body.country;
  if(typeof taxes.get(country) === 'undefined'){
    res.status(400).end();
    return;
  }


  switch (country) {
   /*case 'SK':
      if(total<= 2000){
        total = total + total*taxes.get(country);
      }
      break;
    case 'BE':
      if(total>2000){
        total = total + total*taxes.get(country);
      }
      break;
    case 'UK':
      total = total + total*taxes.get(country);
      total = total * 0.75;
      break*/
    default:
      total = total + total*taxes.get(country);
      break;
  }

  console.log(total);
  switch (req.body.reduction){
    case 'PAY THE PRICE':
      break;
    case 'STANDARD':
      if(total>=50000){
        total = total * 0.85;
      } else if(total >= 10000) {
        total = total * 0.90;
      } else if(total >= 7000) {
        total = total *0.93;
      } else if(total >= 5000) {
        total = total * 0.95;
      } else if(total >=1000){
        total = total * 0.97;
      }
      break;
      case 'HALF PRICE':
        total = total/2;
        break;
      default :
        res.status(400).end();
        return;
  }
  console.log(total);
  res.json({total});

};

exports.feedback = function feedback(req, res, next) {
  console.info("FEEDBACK:", req.body.type, req.body.content);
  next();
};
